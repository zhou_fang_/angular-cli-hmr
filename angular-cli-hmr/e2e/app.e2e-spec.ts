import { AngularCliHmrPage } from './app.po';

describe('angular-cli-hmr App', () => {
  let page: AngularCliHmrPage;

  beforeEach(() => {
    page = new AngularCliHmrPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
